<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@include file="/WEB-INF/jsp/init.jsp" %>
<html>
<head>
    <title><fmt:message key="offer.title"/></title>
</head>
<body>
<div class="site-wrapper">
    <h2 align="center"><fmt:message key="offer.title"/></h2>

    <form class="form-horizontal" role="form" action="<c:url value="/offer"/>" method="post">
        <div class="form-group ">
            <div class="col-lg-11">
                <input class="form-control " name="query" value="${query}">
            </div>
            <input type="submit" class="btn btn-primary" value="<fmt:message key="offer.search"/>">
        </div>
    </form>

    </br>
    <c:forEach items="${pagedData.list}" var="offer">
        <div class="post-wrapper">
            <h4 align="center">${offer.title}</h4>
            </br>
            <div>${offer.description}</div>
        </div>
    </c:forEach>
    <c:if test="${pagedData.navigationInfo.pageCount>1}">
        <div align="center">
            <ul class="pagination pagination">
                <c:if test="${!pagedData.navigationInfo.firstPage}">
                    <li><a href="<c:url value="/offer?page=0&query=${query}"/>"><fmt:message key="pagination.first"/></a></li>
                </c:if>
                <c:forEach var="i" items="${pagedData.navigationInfo.indexList}">
                    <li><a href="<c:url value="/offer?page=${i+1}&query=${query}"/>">${i+2}</a></li>
                </c:forEach>
                <c:if test="${!pagedData.navigationInfo.lastPage}">
                    <li>
                        <a href="<c:url value="/offer?page=${pagedData.navigationInfo.pageCount -1}&query=${query}"/>"><fmt:message key="pagination.last"/></a>
                    </li>
                </c:if>
            </ul>
        </div>
    </c:if>
</div>
</body>
</html>

