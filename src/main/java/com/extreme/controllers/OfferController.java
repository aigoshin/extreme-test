package com.extreme.controllers;


import com.extreme.services.OfferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Requests mapping for Offer
 */
@Controller
public class OfferController {

    @Autowired
    private OfferService offerService;


    @RequestMapping("/offer")
    public String getAllAdverts(Model model, @RequestParam(value = "page", defaultValue = "0", required = false) int page,
                                @RequestParam(value = "query", required = false) String query) {
        model.addAttribute("pagedData", offerService.getOffers(page, query));
        model.addAttribute("query", query);
        return "/offer/show";
    }

}
