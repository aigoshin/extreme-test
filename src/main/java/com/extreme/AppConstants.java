package com.extreme;

/**
 * Application constants
 */
public class AppConstants {
    public static final String BASE_URL = "http://www.zoo-zoo.ru";
    public static final String USER_AGENT = "Mozilla";
    public static final String OFFER_LIST_CSS_CLASS = "offerlist";
    public static final String OFFER_PANEL_CSS_CLASS = "offer_panel";
    public static final String OFFER_DESCRIPTION_CSS_CLASS = "descr";
    public static final Integer OFFER_MAX_RESULT = 4;
}
