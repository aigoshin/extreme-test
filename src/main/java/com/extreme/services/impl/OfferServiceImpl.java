package com.extreme.services.impl;


import com.extreme.AppConstants;
import com.extreme.persistence.DaoFacade;
import com.extreme.persistence.models.Offer;
import com.extreme.services.OfferService;
import com.extreme.utils.PagedView;
import org.apache.commons.lang.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;

/**
 * Implements methods to execute business logic for offers
 */
@Service
public class OfferServiceImpl implements OfferService {

    @Autowired
    private DaoFacade daoFacade;

    @Override
    @Transactional(readOnly = true)
    public PagedView<Offer> getOffers(Integer page, String query) {

        PagedView<Offer> view = new PagedView();

        view.getNavigationInfo().setCurrentPage(page);
        view.getNavigationInfo().setMaxIndices(AppConstants.OFFER_MAX_RESULT);
        view.getNavigationInfo().setPageSize(AppConstants.OFFER_MAX_RESULT);


        if (StringUtils.isEmpty(query)) {
            view.setList(daoFacade.getOfferDao().getPagedOffers(page));
            view.getNavigationInfo().setRowCount(daoFacade.getOfferDao().getOffersCount());
        } else {
            view.setList(daoFacade.getOfferDao().searchOffers(query, page));
            view.getNavigationInfo().setRowCount(daoFacade.getOfferDao().getSearchOffersCount(query));
        }

        return view;
    }


    @Override
    @Transactional(readOnly = false)
    public void refreshOffers() {

        try {
            List<Offer> offers = new LinkedList<>();

            Document document = Jsoup.connect(AppConstants.BASE_URL).userAgent(AppConstants.USER_AGENT).get();
            Element offersArea = document.getElementsByClass(AppConstants.OFFER_LIST_CSS_CLASS).last();
            Elements offerList = offersArea.getElementsByClass(AppConstants.OFFER_PANEL_CSS_CLASS);

            for (Element offer : offerList) {
                offers.add(bindOffer(offer.id(), offer.getElementsByTag("h2").first().text(),
                        offer.getElementsByClass(AppConstants.OFFER_DESCRIPTION_CSS_CLASS).first().text()));
            }
            daoFacade.getOfferDao().bulkSaveOrUpdate(offers);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Offer bindOffer(String uuid, String title, String description) {
        Offer offer = new Offer();
        offer.setOfferUuid(uuid);
        offer.setTitle(title);
        offer.setDescription(description);
        return offer;
    }

}
