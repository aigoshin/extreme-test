package com.extreme.services;


import com.extreme.persistence.models.Offer;
import com.extreme.utils.PagedView;

import java.util.Collection;
import java.util.List;

/**
 * Interface defines methods to execute business logic for offers
 */
public interface OfferService {

    PagedView<Offer> getOffers(Integer page, String query);

    void refreshOffers();
}

