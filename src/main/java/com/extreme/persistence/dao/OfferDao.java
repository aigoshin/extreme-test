package com.extreme.persistence.dao;


import com.extreme.persistence.models.Offer;

import java.util.List;

/**
 * Defines all operations with Offer entities
 */
public interface OfferDao extends GenericDao<Offer, Long> {

    List<Offer> getPagedOffers(Integer page);

    Long getOffersCount();

    Long getSearchOffersCount(String query);

    List<Offer> searchOffers(String query, Integer page);
}
