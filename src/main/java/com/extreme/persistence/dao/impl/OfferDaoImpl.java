package com.extreme.persistence.dao.impl;


import com.extreme.AppConstants;
import com.extreme.persistence.dao.OfferDao;
import com.extreme.persistence.models.Offer;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.FuzzyQuery;
import org.hibernate.Query;
import org.hibernate.search.FullTextQuery;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Implements methods for Offer entities
 */
@Repository
public class OfferDaoImpl extends GenericDaoImpl<Offer, Long> implements OfferDao {

    @Override
    public List<Offer> getPagedOffers(Integer page) {
        Query query = getSession().createQuery("from Offer ");
        query.setFirstResult(page * AppConstants.OFFER_MAX_RESULT);
        query.setMaxResults(AppConstants.OFFER_MAX_RESULT);
        return (List<Offer>) query.list();
    }

    @Override
    public Long getOffersCount() {
        Query query = getSession().createQuery("select count(*) from Offer ");
        return (Long) query.uniqueResult();
    }

    @Override
    public Long getSearchOffersCount(String query) {
        FullTextSession fullTextSession = Search.getFullTextSession(getSession());

        try {
            fullTextSession.createIndexer().startAndWait();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        org.apache.lucene.search.Query luceneQuery = new FuzzyQuery(new Term("title", query));
        FullTextQuery fullTextQuery = fullTextSession.createFullTextQuery(luceneQuery, Offer.class);
        return Long.valueOf(fullTextQuery.getResultSize());

    }

    @Override
    public List<Offer> searchOffers(String query, Integer page) {
        FullTextSession fullTextSession = Search.getFullTextSession(getSession());

        try {
            fullTextSession.createIndexer().startAndWait();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        QueryBuilder builder = fullTextSession.getSearchFactory().buildQueryBuilder().forEntity(Offer.class).get();

        org.apache.lucene.search.Query luceneQuery = builder
                .keyword()
                .onFields("description", "title")
                .matching(query)
                .createQuery();

        Query hibernateQuery = fullTextSession.createFullTextQuery(luceneQuery, Offer.class);
        hibernateQuery.setFirstResult(page * AppConstants.OFFER_MAX_RESULT);
        hibernateQuery.setMaxResults(AppConstants.OFFER_MAX_RESULT);
        return (List<Offer>) hibernateQuery.list();
    }
}
