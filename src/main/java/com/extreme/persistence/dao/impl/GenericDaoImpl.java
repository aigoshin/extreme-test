package com.extreme.persistence.dao.impl;

import com.extreme.persistence.dao.GenericDao;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Implements common operations with Data Access Objects (can be more dao interfaces)
 */
@Repository
public abstract class GenericDaoImpl<T, ID extends Serializable> implements GenericDao<T, ID> {

    @Autowired
    private SessionFactory sessionFactory;

    protected Class<T> modelClass;

    public GenericDaoImpl() {
        modelClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    protected Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public Collection<T> getAll() {
        return getSession().createCriteria(getModelClass()).list();
    }

    @Override
    public void flush() {
        getSession().flush();
    }

    @Override
    public T get(ID p_key) {
        return (T) getSession().get(getModelClass(), p_key);
    }

    @Override
    public void saveOrUpdate(T p_entity) {
        getSession().saveOrUpdate(p_entity);
    }

    @Override
    public void delete(T p_entity) {
        getSession().delete(p_entity);
    }

    @Override
    public T merge(T p_entity) {
        return (T) getSession().merge(p_entity);
    }

    @Override
    public void refresh(T p_entity) {
        getSession().refresh(p_entity);
    }

    @Override
    public Collection<T> bulkSaveOrUpdate(Collection<T> p_entities) {
        List<T> savedEntities = new ArrayList<T>();


        if (!CollectionUtils.isEmpty(p_entities)) {
            Integer batchSize = 20;
            if (sessionFactory != null && sessionFactory.getClass().equals(LocalSessionFactoryBean.class)) {
                batchSize = Integer.valueOf(((LocalSessionFactoryBean) sessionFactory).
                        getHibernateProperties().getProperty("hibernate.jdbc.batch_size", "20"));
            }

            int count = 0;
            for (T entity : p_entities) {
                entity = merge(entity);
                saveOrUpdate(entity);
                savedEntities.add(entity);
                if (++count % batchSize == 0) {
                    getSession().flush();
                    getSession().clear();
                }
            }
        }
        getSession().flush();
        getSession().clear();


        return savedEntities;
    }

    protected Class<T> getModelClass() {
        return modelClass;
    }

}
