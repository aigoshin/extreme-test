package com.extreme.persistence.dao;


import java.io.Serializable;
import java.util.Collection;

/**
 * Defines common operations with Data Access Objects
 */
public interface GenericDao<T, ID extends Serializable> {

    Collection<T> getAll();

    void flush();

    T get(ID key);

    void saveOrUpdate(T entity);

    void delete(T entity);

    T merge(T entity);

    void refresh(T entity);

    Collection<T> bulkSaveOrUpdate(Collection<T> entities);

}
