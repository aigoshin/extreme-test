package com.extreme.persistence.models;


import org.hibernate.search.annotations.*;

import javax.persistence.*;


@Entity
@Indexed
@Table(name = "offer")
public class Offer {

    @Id
    @Column(name = "offerUuid")
    private String offerUuid;

    @Field(index = Index.YES, analyze = Analyze.YES, store = Store.NO)
    @Column(name = "title")
    private String title;

    @Lob
    @Field(index = Index.YES, analyze = Analyze.YES, store = Store.NO)
    @Column(name = "description")
    private String description;

    public String getOfferUuid() {
        return offerUuid;
    }

    public void setOfferUuid(String offerUuid) {
        this.offerUuid = offerUuid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
