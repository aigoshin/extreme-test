package com.extreme.persistence;


import com.extreme.persistence.dao.OfferDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Facade for all DAO interfaces (can be more interfaces)
 *
 */
@Component
public class DaoFacade {

    @Autowired
    private OfferDao offerDao;

    public OfferDao getOfferDao() {
        return offerDao;
    }

}
