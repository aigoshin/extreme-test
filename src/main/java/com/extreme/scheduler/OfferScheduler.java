package com.extreme.scheduler;

import com.extreme.services.OfferService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 * Scheduler for refreshing offers
 */
@Service
public class OfferScheduler implements InitializingBean {

    @Autowired
    private OfferService offerService;

    @Scheduled(cron = "${cron.expression}")
    public void execute() {
        offerService.refreshOffers();
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        offerService.refreshOffers();
    }
}
