package com.extreme.utils;

import java.util.List;

/**
 * Util for pagination
 */
public class PagedView<T> {

    private NavigationInfo navigationInfo = new NavigationInfo();
    private List<T> list;

    public NavigationInfo getNavigationInfo() {
        return navigationInfo;
    }

    public void setNavigationInfo(NavigationInfo navigationInfo) {
        this.navigationInfo = navigationInfo;
    }

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }

}